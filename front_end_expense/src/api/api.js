import axios from "axios";
import util from "../api/util.js";

export const BASE_URL = `https://localhost:8081/api/`;
export const HTTP = axios.create({
  baseURL: BASE_URL,
  validateStatus: false,
  headers: {
    accept: "application/json",
  },
});

export const LOGIN_REQUEST = (params) => {
  return HTTP.post(`auth/signin`, params).then((res) => res.data);
};
export const POST_UTILISATEUR = (params) => {
  return HTTP.post(`auth/signup`, params).then((res) => res.data);
};
export const POST_GROUP = (params) => {
  return HTTP.post(`groups`, params, util.getToken()).then(
    (res) => res.data
  );
};
export const GET_GROUPS = () => {
  return HTTP.get(`groups`, util.getToken()).then(
    (res) => res.data
  );
};

export const GET_USERS = () => {
  return HTTP.get(`users`, util.getToken()).then(
    (res) => res.data
  );
};

export const ADD_MEMBER_TO_GROUP = (groupId, userId) => {
  return HTTP.post(`groups/${groupId}/members/${userId}`, {}, util.getToken()).then(
    (res) => res.data
  );
};

export const GET_GROUP_MEMBERS = (groupId) => {
  return HTTP.get(`groups/${groupId}/members`, util.getToken()).then(
    (res) => res.data
  );
};

export const POST_EXPENSE = (params) => {
  return HTTP.post(`expenses`, params, util.getToken()).then(
    (res) => res.data
  );
};

export const GET_EXPENSES_BY_OWNER = (userId) => {
  return HTTP.get(`expenses/owner/${userId}`, util.getToken()).then(
    (res) => res.data
  );
};

export const GET_EXPENSES_OF_USERS = (userId) => {
  return HTTP.get(`expenses/user/${userId}`, util.getToken()).then(
    (res) => res.data
  );
};

export const PAY_EXPENSE = (expenseId, userId) => {
  return HTTP.put(`expenses/${expenseId}/users/${userId}/pay`, {}, util.getToken()).then(
    (res) => res.data
  );
};


export const POST_GROUP_KEYS = (params) => {
  return HTTP.post(`groupkeys`, params, util.getToken()).then(
    (res) => res.data
  );
};

export const GET_GROUP_KEYS = (groupId) => {
  return HTTP.get(`groupkeys/${groupId}`, util.getToken()).then(
    (res) => res.data
  );
};


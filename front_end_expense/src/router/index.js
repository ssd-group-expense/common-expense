import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from '../views/LoginView.vue'
import RegisterView from '../views/RegisterView.vue'
import GroupView from '../views/GroupView.vue'
import ExpenseView from '../views/ExpenseView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
    meta: { requiresAuth: true }
  },
  {
    path: '/groups', // define the path for the login page
    name: 'group', // give it a name
    component: GroupView,
    meta: { requiresAuth: true } // specify the LoginView component
  },
  {
    path: '/login', // define the path for the login page
    name: 'login', // give it a name
    component: LoginView // specify the LoginView component
  },
  {
    path: '/register', // define the path for the login page
    name: 'register', // give it a name
    component: RegisterView // specify the LoginView component
  },
  {
    path: '/expenses', // define the path for the login page
    name: 'expense', // give it a name
    component: ExpenseView,
    meta: { requiresAuth: true } // specify the LoginView component
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!sessionStorage.getItem('token')) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router

const db = require("../models");
const { expense: Expense, user: User, expenseMember: ExpenseMember  } = db;
const Op = db.Sequelize.Op;

const getPagination = (page, size) => {
    const limit = size ? +size : 3;
    const offset = page ? page * limit : 0;
  
    return { limit, offset };
  };


  const getPagingData = (data, page, limit) => {
    const { count: totalItems, rows: expenseMember } = data;
    const currentPage = page ? +page : 0;
    const totalPages = Math.ceil(totalItems / limit);
  
    return { totalItems, expenseMember, totalPages, currentPage };
  };


 // Pay balance for a specific expense
exports.payBalance = (req, res) => {
    const { expenseId, userId } = req.params;

    ExpenseMember.findOne({
        where: { expenseId: expenseId, userId: userId }
      })
        .then((expenseMember) => {
          if (expenseMember) {
            // Update the Expense instance
            Expense.findByPk(expenseId)
              .then((expense) => {
                if (expense) {
                  expense.update({
                    amount: expense.amount - expenseMember.balance
                  })
                  .then(() => {
                    // Update the ExpenseMember instance
                    expenseMember.update({ balance: 0, status: "paid" })
                      .then(() => {
                        res.send({
                          message: "Balance was paid successfully.",
                        });
                      });
                  });
                } else {
                  res.status(404).send({
                    message: `Expense with id=${expenseId} not found.`,
                  });
                  return;
                }
              });
          } else {
            res.send({
              message: `Cannot pay balance for Expense with id=${expenseId} and User with id=${userId}. Maybe ExpenseMember was not found!`,
            });
          }
        })
        .catch((err) => {
          console.error(err);
          res.status(500).send({
            message: "Error paying balance for Expense with id=" + expenseId,
          });
        });
  }; 


  // Consult balance per expense
exports.findBalance = (req, res) => {
    const { expenseId, userId } = req.params;
  
    ExpenseMember.findOne({
      where: { expenseId: expenseId, userId: userId }
    })
      .then((data) => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).send({
            message: `Cannot find ExpenseMember with Expense id=${expenseId} and User id=${userId}.`,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: "Error retrieving ExpenseMember with Expense id=" + expenseId + " and User id=" + userId,
        });
      });
  };

  // List the history of payments
exports.findPaymentHistory = (req, res) => {
    const userId = req.params.userId;
  
    ExpenseMember.findAll({
      where: { userId: userId },
      order: [['updatedAt', 'DESC']]
    })
      .then((data) => {
        res.send(data);
      })
      .catch((err) => {
        res.status(500).send({
          message: err.message || "Some error occurred while retrieving payment history.",
        });
      });
  };
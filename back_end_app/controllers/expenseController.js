const db = require("../models");
const { expense: Expense, group: Group, user: User, expenseMember: ExpenseMember } = db;
const Op = db.Sequelize.Op;

const getPagination = (page, size) => {
  const limit = size ? +size : 3;
  const offset = page ? page * limit : 0;

  return { limit, offset };
};

const getPagingData = (data, page, limit) => {
  const { count: totalItems, rows: expense } = data;
  const currentPage = page ? +page : 0;
  const totalPages = Math.ceil(totalItems / limit);

  return { totalItems, expense, totalPages, currentPage };
};

// Create an expense
exports.create = (req, res) => {
  // Validate request
  if (!req.body.amount || !req.body.paidBy || !req.body.groupId) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
    return;
  }

  // Create an Expense
  const expense = {
    title: req.body.title,
    description: req.body.description,
    date: req.body.date,
    amount: req.body.amount,
    paidBy: req.body.paidBy,
    groupId: req.body.groupId,
  };

  // Save Expense in the database
  Expense.create(expense)
    .then((data) => {
      // Retrieve the group associated with the expense
      Group.findByPk(req.body.groupId)
        .then((group) => {
          // Get all members of the group
          group.getUsers().then((members) => {
            // Create an ExpenseMember instance for each member
            for (let member of members) {
              ExpenseMember.create({
                userId: member.id,
                expenseId: data.id,
                balance: data.amount / members.length,
              });
            }

            res.send(data);
          });
        });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Expense.",
      });
    });
};

// Update an expense
exports.update = (req, res) => {
  const id = req.params.id;

  Expense.update(req.body, {
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Expense was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update Expense with id=${id}. Maybe Expense was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating Expense with id=" + id,
      });
    });
};

// Delete an expense
exports.delete = (req, res) => {
  const id = req.params.id;

  Expense.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Expense was deleted successfully!",
        });
      } else {
        res.send({
          message: `Cannot delete Expense with id=${id}. Maybe Expense was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete Expense with id=" + id,
      });
    });
};

// List all expenses linked to the current user
exports.findAll = (req, res) => {
  const { page, size, userId } = req.query;
  var condition = userId ? { userId: { [Op.eq]: `${userId}` } } : null;

  const { limit, offset } = getPagination(page, size);

  Expense.findAndCountAll({ where: condition, limit, offset })
    .then((data) => {
      const response = getPagingData(data, page, limit);
      res.send(response);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving expenses.",
      });
    });
};

// exports.findAllByUser = (req, res) => {
//   const userId = req.params.userId;

//   ExpenseMember.findAll({
//     where: { userId: userId },
//     include: [
//       {
//         model: Expense,
//         as: 'expense',
//       },
//     ],
//   })
//     .then((data) => {
//       const expenses = data.map((expenseMember) => expenseMember.expense);
//       res.send(expenses);
//     })
//     .catch((err) => {
//       res.status(500).send({
//         message: err.message || "Some error occurred while retrieving expenses.",
//       });
//     });
// };

exports.findAllByUser = (req, res) => {
  const userId = req.params.userId;

  ExpenseMember.findAll({
    where: { userId: userId },
    include: [
      {
        model: Expense,
        as: 'expense',
      },
    ],
  })
    .then((data) => {
      const expenses = data.map((expenseMember) => {
        const expense = expenseMember.expense;
        const balance = expenseMember.balance;
        const status = expenseMember.status;
        return { ...expense.dataValues, balance, status };
      });
      res.send(expenses);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving expenses.",
      });
    });
};

exports.findAllByOwner = (req, res) => {
  console.log(req.params);
  const paidBy = req.params.userId;

  Expense.findAll({ where: { paidBy: paidBy } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving expenses.",
      });
    });
};






const db = require("../models");
// const GroupKey = db.groupKey;
const { groupKey: GroupKey } = db;

exports.createGroupKeys = (req, res) => {
  const { groupId, keys } = req.body;

  // Create a promise for each key creation
  const promises = keys.map(key => {
    return GroupKey.create({
      groupId: groupId,
      userId: key.userId,
      encryptedKey: key.encryptedGroupKey
    });
  });

  // Wait for all promises to resolve
  Promise.all(promises)
    .then(() => {
      res.send({ message: "Group keys were created successfully!" });
    })
    .catch(err => {
      res.status(500).send({ message: err.message });
    });
};;

  // exports.getGroupKeys = (req, res) => {
  //   const groupId = req.params.groupId;
  
  //   GroupKey.findAll({ where: { groupId: groupId } })
  //     .then(data => {
  //       res.send(data);
  //     })
  //     .catch(err => {
  //       res.status(500).send({
  //         message: err.message || "Some error occurred while retrieving group keys."
  //       });
  //     });
  // };

  exports.getGroupKeys = (req, res) => {
    const groupId = req.params.groupId;
  
    GroupKey.findAll({ where: { groupId: groupId } })
    .then(data => {
      res.json({ groupKeys: data });  // Wrap the data in an object
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving group keys."
      });
    });
  };


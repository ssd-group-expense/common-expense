module.exports = (sequelize, Sequelize) => {
    const GroupKey = sequelize.define("groupKeys", {
      groupId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'groups',
          key: 'id'
        }
      },
      userId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'users',
          key: 'id'
        }
      },
      encryptedKey: {
        type: Sequelize.TEXT,
      }
    });
  
    return GroupKey;
  };
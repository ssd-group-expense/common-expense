module.exports = (sequelize, Sequelize) => {
  const Expense = sequelize.define("expenses", {
    title: {
      type: Sequelize.STRING,
    },
    description: {
      type: Sequelize.STRING,
    },
    amount: {
      type: Sequelize.FLOAT,
    },
    date: {
      type: Sequelize.DATE,
    },
    paidBy: {
      type: Sequelize.INTEGER,
      references: {
        model: 'users', // name of the table, not the model
        key: 'id'
      }
    },
    groupId: {
      type: Sequelize.INTEGER,
      references: {
        model: 'groups', // name of the table, not the model
        key: 'id'
      }
    }
  });

  return Expense;
};

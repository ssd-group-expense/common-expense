const { authJwt } = require("../midllewares/index.js");
const controller = require("../controllers/groupKeyController.js");

module.exports = function (app) {
    app.use(function (req, res, next) {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });

      // Create new group keys
    app.post("/api/groupkeys", [authJwt.verifyToken], controller.createGroupKeys);

  // Retrieve all group keys for a specific group
     app.get("/api/groupkeys/:groupId", [authJwt.verifyToken], controller.getGroupKeys);


};